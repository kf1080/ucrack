package com.virjar.ucrack.plugin.unpack;

/**
 * Created by virjar on 2018/3/12.<br>
 * 脱壳器,请注意，目前三代壳只支持在dalvik上面脱壳。不支持art模式（这意味着Android5.0以上的版本的手机，暂不支持脱壳），一代壳可以在art和dalvik上面脱壳
 */
public class Dumper {

    /**
     * 对一代壳进行脱壳，hook dvm函数，不需要传递classloader<br>
     * 一代壳没有方法抽取，是对整个dex进行加密，
     */
    public static native void dumpVersion1();
}