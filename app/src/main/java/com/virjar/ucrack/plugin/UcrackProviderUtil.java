package com.virjar.ucrack.plugin;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import com.virjar.ucrack.contentprovider.UcrackProvider;

import java.util.HashMap;
import java.util.Map;

/**
 * @author lei.X
 * @date 2019/4/15 2:52 PM
 */
public class UcrackProviderUtil {

    public static final String tag = "UcrackProviderUtil";

    public Context mContext;

    private static volatile UcrackProviderUtil singleton = null;

    private static final String URL_PREFIX = "content://com.virjar.ucrack.contentprovider.ucrack/";


    private UcrackProviderUtil(Context mContext) {
        this.mContext = mContext;
    }

    public static UcrackProviderUtil getSingleton(Context context) {
        synchronized (UcrackProviderUtil.class) {
            if (singleton == null) {
                singleton = new UcrackProviderUtil(context);
            }
        }
        return singleton;
    }

    private Uri public_data = Uri.parse(URL_PREFIX + UcrackProvider.PUBLICDATA_TABLE_NAME);


    /**
     * 更新开关的状态
     *
     * @param appName
     * @param propertity
     * @param switchBoolean
     */
    public void updateAppPropertity(String appName, String propertity, Boolean switchBoolean) {

        ContentResolver resolver = mContext.getContentResolver();
        ContentValues values = new ContentValues();
        values.put("switch", switchBoolean);
        try {
            int updateRst = resolver.update(public_data, values, " app_name =? and propertity =?", new String[]{appName, propertity});
            if (updateRst == 0) { //不存在更新则说明该值不存在
                values.put("app_name", appName);
                values.put("propertity", propertity);
                resolver.insert(public_data, values);
            }
        } catch (Exception e) {
            Log.e(tag, e.toString());
        }

    }


    /**
     * 查询app已经注册的所有的属性
     *
     * @param appName
     * @return
     */
    public Map<String, Boolean> queryMapWithAppName(String appName) {

        Map<String, Boolean> configMap = new HashMap<>();

        String selection = "app_name=?";
        String selections[] = new String[]{appName};
        ContentResolver resolver = mContext.getContentResolver();
        Cursor cursor = resolver.query(public_data, null, selection, selections, null);
        if (cursor == null) {
            return configMap;
        }
        while (cursor.moveToNext()) {
            String propertity = cursor.getString(cursor.getColumnIndex("propertity"));
            int switchProperties = cursor.getInt(cursor.getColumnIndex("switch"));
            configMap.put(propertity, switchProperties == 1);
        }
        cursor.close();
        return configMap;
    }

    /**
     * 查询app的propertity是否存在
     *
     * @param appName
     * @param propertity
     * @return
     */
    public boolean queryAppPropertity(String appName, String propertity) {

        String selection = "app_name=? and propertity=?";
        String selections[] = new String[]{appName, propertity};

        ContentResolver resolver = mContext.getContentResolver();
        Cursor cursor = resolver.query(public_data, null, selection, selections, null);

        if (cursor.getCount() == 0) {  // 不存在则默认为 0
            Log.i(tag, appName + ":" + propertity + ": not exist");
            cursor.close();
            return false;
        } else {  // 存在这个属性则查看数据库为0 或者 1
            cursor.moveToLast();
            int switchProperties = cursor.getInt(cursor.getColumnIndex("switch"));
            Log.i(tag, appName + ":" + propertity + "--" + switchProperties);
            cursor.close();
            return switchProperties == 1;
        }

    }


    /**
     * 给app注册开关属性
     * "CREATE TABLE IF NOT EXISTS " + PUBLICDATA_TABLE_NAME + "(_id INTEGER PRIMARY KEY AUTOINCREMENT," + " app_name TEXT,propertity TEXT,switch INTEGER)"
     *
     * @param appName
     * @param propertity
     * @param boolValue
     */
    public void insertAppPropertity(String appName, String propertity, Boolean boolValue) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_name", appName);
        contentValues.put("propertity", propertity);
        contentValues.put("switch", boolValue);
        insert(contentValues);
    }

    private void insert(ContentValues contentValues) {
        ContentResolver resolver = mContext.getContentResolver();
        resolver.insert(public_data, contentValues);
    }

    /**
     * 删除
     */
    public void delete() {
        ContentResolver resolver = mContext.getContentResolver();
        resolver.delete(public_data, null, null);
    }


}
