## hook base工具
Android 破解的hook工具，集成一些帮助破解的常用功能，如自动网络抓包、网络堆栈爆破、文件日志、webview调试环境，脱壳工具

该工具的前身，是 https://gitee.com/virjar/xposedhooktool ，经过开源-闭源-再开源，ucrack由一个简单的工具演化为一个完善的工具集了


#### 申明
**ucrack中，不会维护脱壳机功能，其他大多功能维护完善，在ucrack之上，进行了数十个Android App的破解**

**受限于商业限制，各app破解功能也不会开源**

**如果商业合作，请联系qq：819154316**

### 参考项目
如果你无法完成协议破解，可以尝试hermes项目，[hermesAgent](https://gitee.com/virjar/hermesagent)

#### 捐赠
如果你觉得作者辛苦了，可以的话请我喝杯咖啡
![alipay](doc/img/reward.jpg)

